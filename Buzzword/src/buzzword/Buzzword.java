package buzzword;

import apptemplate.AppTemplate;
import components.AppComponentsBuilder;
import components.AppDataComponent;
import components.AppFileComponent;
import components.AppWorkspaceComponent;
import data.DataManager;
import data.GameDataFile;
import gui.Workspace;

/**
 * Created by jamie on 11/7/2016.
 * @author Jamie Kunzmann
 */
public class Buzzword extends AppTemplate {
    public static void main(String[] args) {
        launch(args);
    }

    public String getFileControllerClass() {
        return "MainController";
    }

    @Override
    public AppComponentsBuilder makeAppBuilderHook() {
        return new AppComponentsBuilder() {
            @Override
            public AppDataComponent buildDataComponent() throws Exception {
                return new DataManager (Buzzword.this);
            }

            @Override
            public AppFileComponent buildFileComponent() throws Exception {
                return new GameDataFile();
            }

            @Override
            public AppWorkspaceComponent buildWorkspaceComponent() throws Exception {
                return new Workspace(Buzzword.this);
            }
        };
    }
}
