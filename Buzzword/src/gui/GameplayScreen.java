package gui;

import apptemplate.AppTemplate;
import buzzword.BuzzwordProperties;
import com.fasterxml.jackson.core.TreeNode;
import components.AppWorkspaceComponent;
import controller.CountdownTimer;
import controller.GameplayController;
import controller.MainController;
import data.DataManager;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;
import sun.reflect.generics.tree.Tree;
import ui.AppGUI;
import ui.AppMessageDialogSingleton;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.locks.ReentrantLock;

import static buzzword.BuzzwordProperties.*;
import static buzzword.BuzzwordProperties.HOME_HEADING_LABEL;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * Created by jamie on 11/7/2016.
 * @author Jamie Kunzmann
 */
public class GameplayScreen extends Workspace {
    AppTemplate app; // the actual application
    AppGUI gui; // the GUI inside which the application sits
    Workspace bwWorkspace;

    private ReentrantLock reentrantlock;

    //controllers
    MainController mainController;
    GameplayController gameplayController; //controller for level ui

    BorderPane gameplayWorkspace; //container for gameplay workspace
    GridPane letterGrid;
    VBox rightSide;

    DoubleProperty heightProp;
    VBox scoreDisplay;
    ScrollPane scoreScroll;
    ScrollBar scrollBar;
    GridPane  scoreGrid;
    HBox timeRemainContainer;
    VBox timeRemainBox;

    //new score containers
    HBox scoreContainer;
    VBox wordsVBox;
    VBox scoresVBox;

    String lettersStr;
    Text lettersGuessed;
    HBox wordProgress;

    VBox targetScoreBox;
    Label targetScoreLabel;
    Label targetScore;
    GridPane totalScore;
    Text totalLabel;
    Text currentScore;
    Label timeRemainingLabel;
    Label timeRemaining;
    Label secondsLabel;
    HBox timeRemainHBox;
    Label modeLabel;
    Label levelLabel;

    Button playPause;
    ImageView playImage;
    ImageView pauseImage;
    ImageView replayImage;
    ImageView nextLevelImage;
    Button replay;
    Button nextLevel;
    HBox footer;

    VBox center; //container for center info
    VBox leftSide;
    boolean isPaused;

    private final int totalTime=60;

    Button[][] letterButtons;
    char[][] letters;

    //buttons
    Button usernameButton;
    Button homeButton; //button to return to homescreen
    private final KeyCodeCombination replayKC= new KeyCodeCombination(KeyCode.R, KeyCombination.SHIFT_ANY);

    public final int LETTER_GRID_WIDTH=4;
    public final int LETTER_GRID_HEIGHT=4;

    public GameplayScreen(AppTemplate initApp){
        app = initApp;
        gui = app.getGUI();

        mainController=(MainController)gui.getFileController();
        gameplayController=mainController.getGameplayController();
        gameplayWorkspace=new BorderPane();
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        initToolbarButtons();

        setupHandlers(); // ... and set up event handling
        scrollHandler();
    }

    public BorderPane getWorkspace(){return gameplayWorkspace;}


    /**
     * initializes the letter grid, labels, target score according to mode and level
     */
    public void initScreen(){
        DataManager dm=(DataManager) app.getDataComponent();
        levelLabel.setText("Level " + dm.getCurrentLevel());
        String mode=dm.getModes().get(dm.getCurrentProfile().getCurrentMode()).getLowercaseName();
        modeLabel.setText(mode);

        boolean isHighestLevel=dm.getCurrentProfile().isHighestLevel(dm.getCurrentLevel());
        //clears any previous data. next level button visible if not highest level
        resetData(!isHighestLevel);

        //must initalize grid before setting target score
        initLetters();
        //initLetterGrid();
        initializeTargetScore(dm);
        letterGrid.setVisible(false);
        playPause.setGraphic(playImage);
    }

    private void initializeTargetScore(DataManager dm){
        int numValidWords=dm.getNumValidWords();
        int level=dm.getCurrentLevel();
        targetScore.setText("" + dm.calculateTargetScore());
    }


    public Pane getScreen(){
        return gameplayWorkspace;
    }
    private void layoutGUI() {

        PropertyManager propertyManager=PropertyManager.getManager();

        letterGrid=new GridPane();
        letterGrid.setAlignment(Pos.CENTER);
        letterGrid.setVisible(false);
        initLetterGrid();

        //mode title
        modeLabel=new Label();
        modeLabel.getStyleClass().setAll(propertyManager.getPropertyValue(GAME_MODE_LABEL_CSS));
        modeLabel.setAlignment(Pos.CENTER);

        //footer
        playPause=new Button();
        playImage=initializeIcon(playImage, PLAY_ICON);
        pauseImage=initializeIcon(pauseImage, PAUSE_ICON);
        playPause=initializeFooterButton(playImage);

        replay=new Button();
        replayImage=initializeIcon(replayImage, REPLAY_ICON);
        replay=initializeFooterButton(replayImage);

        nextLevel=new Button();
        nextLevelImage=initializeIcon(nextLevelImage, ARROW_ICON);
        nextLevel=initializeFooterButton(nextLevelImage);
        nextLevel.setVisible(false);

        footer=new HBox();
        footer.setAlignment(Pos.CENTER);
        footer.setSpacing(10);
        footer.getChildren().addAll(replay, playPause, nextLevel);

        //level label
        levelLabel= new Label();
        levelLabel.getStyleClass().setAll(propertyManager.getPropertyValue(GAME_MODE_LABEL_CSS));
        levelLabel.setAlignment(Pos.CENTER);

        center =new VBox();
        center.setSpacing(30);
        center.setAlignment(Pos.CENTER);
        center.getChildren().addAll(modeLabel, letterGrid, levelLabel, footer);

        //time remaining display
        timeRemainingLabel=new Label("Time Remaining: ");
        timeRemainingLabel.setStyle("-fx-font-size: 16pt;"+"-fx-underline: true;"+"-fx-text-fill: #E8E8E8;");
        timeRemaining = CountdownTimer.getTimeLabel();
        timeRemaining.setTextFill(Color.LIMEGREEN);
        timeRemaining.setStyle("-fx-font-size: 16pt");
        secondsLabel=CountdownTimer.getSecondsLabel();
        secondsLabel.setTextFill(Color.LIMEGREEN);
        secondsLabel.setStyle("-fx-font-size: 16pt");
        timeRemainHBox=new HBox();

        timeRemainHBox.setSpacing(10);
        timeRemainHBox.getChildren().addAll(timeRemaining,secondsLabel);
        timeRemainHBox.setAlignment(Pos.CENTER);
        timeRemainBox=new VBox();
        timeRemainBox.setSpacing(20);
        timeRemainBox.setAlignment(Pos.CENTER);
        timeRemainBox.getChildren().addAll(timeRemainingLabel, timeRemainHBox);


        //word progress
        wordProgress=new HBox();
        wordProgress.setMaxWidth(200);
        wordProgress.setPrefWidth(200);
        wordProgress.setMinHeight(50);
        wordProgress.setPrefHeight(50);
        wordProgress.setStyle("-fx-background-color: #B5E9FF;"+"-fx-border-radius: 10;"+"-fx-background-radius:10");
        wordProgress.setAlignment(Pos.CENTER_LEFT);
        wordProgress.setPadding(new Insets(5,5,5,5));

        lettersGuessed=new Text();
        lettersGuessed.setFill(Color.GREY);
        lettersGuessed.setStyle("-fx-font-size: 16pt;");
        wordProgress.getChildren().add(lettersGuessed);


        //score display
        //new
        scoreContainer=new HBox();
        scoreContainer.setMaxWidth(200);
        scoreContainer.setPrefWidth(200);
        scoreContainer.setMinHeight(200);
        scoreContainer.setStyle("-fx-background-color: #B5E9FF;");
        wordsVBox=new VBox();

        wordsVBox.setMinWidth(150);
        wordsVBox.setSpacing(3);
        scoresVBox=new VBox();
        scoresVBox.setSpacing(3);
        scoreContainer.getChildren().setAll(wordsVBox, scoresVBox);

        scoreGrid=new GridPane();
        scoreGrid.setStyle("-fx-background-color: #B5E9FF;");
        scoreGrid.setHgap(100);
        scoreGrid.setMaxWidth(200);
        scoreGrid.setPrefWidth(200);
        scoreGrid.setMinHeight(400);
        //dummy method for fake score
       // initScoreGrid();
        scoreScroll=new ScrollPane();

        scoreScroll.setPrefWidth(200);
        scoreScroll.setMaxWidth(200);
        scoreScroll.setVmax(200);
        scoreScroll.setPrefHeight(200);
        scoreScroll.setStyle("-fx-background-color: white;");
        scoreScroll.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scoreScroll.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scoreScroll.setContent(scoreContainer);


        totalLabel=new Text("TOTAL");

        totalLabel.setStyle("-fx-text-fill: #000000;"+"-fx-font-size:12pt;");
        currentScore=new Text("0");
        currentScore.setStyle("-fx-text-fill: #000000;"+"-fx-font-size:12pt;");
        totalScore=new GridPane();
        totalScore.setStyle("-fx-background-color: #95C6DB;");
        totalScore.setHgap(101);
        totalScore.setMaxWidth(200);
        totalScore.setPrefWidth(200);
        totalScore.add(totalLabel,0,0);
        totalScore.add(currentScore, 1,0);

        scoreDisplay=new VBox();
        scoreDisplay.getChildren().addAll(scoreScroll, totalScore);
        scoreDisplay.setAlignment(Pos.CENTER);



        //target
        targetScoreLabel =new Label("Target Score: ");
        targetScore=new Label(); //DUMMY PLACE HOLDER

        targetScoreLabel.setStyle("-fx-font-size: 16pt;" +"-fx-underline:true;"+"-fx-text-fill: #54FF62;");
        targetScore.setStyle("-fx-font-size: 16pt;" +"-fx-text-fill: #54FF62;");

        targetScoreBox=new VBox();
        targetScoreBox.setAlignment(Pos.CENTER);
        targetScoreBox.getChildren().addAll(targetScoreLabel, targetScore);

        rightSide=new VBox();
        rightSide.setAlignment(Pos.CENTER);
        rightSide.setSpacing(30);
        rightSide.setPrefWidth(300);

        //rightSide.setFillWidth(true);
        rightSide.getChildren().addAll(wordProgress, scoreDisplay, targetScoreBox);


        leftSide=new VBox();
        leftSide.setAlignment(Pos.CENTER);
        leftSide.setPadding(new Insets(0,0,0,50));
        leftSide.prefWidthProperty().bind(rightSide.prefWidthProperty());
        leftSide.getChildren().add(timeRemainBox);
        gameplayWorkspace.setCenter(center);
        gameplayWorkspace.setRight(rightSide);
        gameplayWorkspace.getRight().minWidth(100);

        Region leftPad = new Region();
        leftPad.prefWidthProperty().bind(rightSide.prefWidthProperty());
        gameplayWorkspace.setLeft(leftSide);



    }
    private Button initializeFooterButton(ImageView iv){
        Button b=new Button();
        b.setAlignment(Pos.CENTER);
        b.setGraphic(iv);
        b.setMaxSize(30,30);
        b.setStyle("-fx-background-color: transparent;");
        return b;
    }

    private ImageView initializeIcon(ImageView imageView, BuzzwordProperties icon){
        PropertyManager propertyManager=PropertyManager.getManager();
        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            try {
                throw new FileNotFoundException("Image resources folder does not exist.");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        try {
            try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(icon.toString())))) {
                Image buttonImage = new Image(imgInputStream);
                imageView=new ImageView(buttonImage);
                imageView.setFitHeight(30);
                imageView.setFitWidth(30);
            } catch (URISyntaxException e) {
                e.printStackTrace();
                System.exit(1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageView;

    }

    public void pauseOrEndGraphics(){
        letterGrid.setVisible(false);
        playPause.setGraphic(playImage);
    }
    public void resumeOrStartGraphics(){
        letterGrid.setVisible(true);
        playPause.setGraphic(pauseImage);
    }
    private void initLetters(){
        DataManager dm= (DataManager) app.getDataComponent();
        int numWords=0;
        while(numWords<6){
            letters=dm.generateLetterGrid();
            //i, j represents coordinates of the starting point of the letter paths currently being searched
            numWords=dm.getNumValidWords();
        }

        setLetterButtonsText();



    }
    /**
     * precondition: Dimensions of letter 2D array must be the same as the letterButtons 2D array
     */
    private void setLetterButtonsText(){for(int i=0;i<letterButtons.length;i++) {
        for (int j = 0; j < letterButtons[0].length; j++) {
            letterButtons[i][j].setText(Character.toString(letters[i][j]));
        }
    }}

    private void initLetterButtons(){
      //  initLetters();
        letterButtons=new Button[LETTER_GRID_HEIGHT][LETTER_GRID_WIDTH];

        Button button;

        for(int i=0;i<letterButtons.length;i++)
        {

            for(int j=0;j<letterButtons[0].length;j++)
            {
                    //Character.toString(letters[i][j])
                    button= new Button();
                    button.setPrefSize(50, 50);
                    button.setStyle("-fx-font-size: 12pt;" +"-fx-background-color: #D6B0D1;");
                    button.setShape(new Circle(25));
                    letterButtons[i][j]=button;


            }


        }
    }
    public void scrollHandler(){
        heightProp=new SimpleDoubleProperty();
        heightProp.bind(scoreContainer.heightProperty());
        heightProp.addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observed, Object a, Object b){
                scoreScroll.setVvalue(scoreScroll.getVmax());

            }
        });
    }

    public void setLetterGridVisibility(boolean val){
        letterGrid.setVisible(val);
    }

    private void initLetterGrid(){
        initLetterButtons();
        //change this later
        Line line;
       // letterGrid=new GridPane();
        //letterGrid.setHAlignment(Pos.CENTER);

        int buttonRow=0;
        for(int i=0;i<LETTER_GRID_HEIGHT*2-1;i++){
            int buttonCol=0;
            for(int j=0;j<LETTER_GRID_WIDTH*2-1;j++){
                //places letter button if row and column index both even
                if(i%2==0&&j%2==0) {

                    letterGrid.add(letterButtons[buttonRow][buttonCol], j,i);
                    buttonCol++;
                }else if(i%2==0){
                    line=new Line(0,25,25,25);
                    line.setStyle("-fx-stroke: #9C9C9C;");
                    letterGrid.add(line, j,i);
                }
                else if(i%2==1&&j%2==0) {
                    line=new Line(25,0,25,25);
                    line.setStyle("-fx-stroke: #9C9C9C;");
                    letterGrid.add(line, j, i);
                    letterGrid.setHalignment(line, HPos.CENTER);
                }


            }
            if(i%2==0) buttonRow++;
        }

    }

    public Button getPlayPauseButton(){return playPause;}
    public Text getLettersGuessed(){return lettersGuessed;}
    private void setupHandlers()
    {
        playPause.setOnAction(e-> gameplayController.handlePlayPauseRequest(this));
        //startGame.setOnMouseClicked(e -> homeController.start());
        //hintButton.setOnMouseClicked(e->homeController.revealHint(this));
        homeButton.setOnAction(e->gameplayController.handleHomeRequest());
        usernameButton.setOnAction(e->mainController.handleAccountRequest());
        setupBuzzwordBoardHandlers();
        replay.setOnAction(e->gameplayController.handleReplayRequest(this));
        nextLevel.setOnAction(e->gameplayController.handleNextLevelRequest(this));

        /**app.getGUI().getPrimaryScene().setOnKeyReleased((KeyEvent e) -> {
            if (replayKC.match(e))
                gameplayController.handleReplayRequest(this);
        });
         */
    }

    private void setupBuzzwordBoardHandlers(){
        for(int i=0;i<letterButtons.length;i++){
            for(int j=0;j<letterButtons[0].length;j++){
                int finalI = i;
                int finalJ = j;
                letterButtons[i][j].setOnDragDetected(e->{
                    letterButtons[finalI][finalJ].startFullDrag();

                });
                letterButtons[i][j].setOnMouseDragEntered(e->{
                    gameplayController.handleDragOverLetter(letterButtons[finalI][ finalJ],finalI, finalJ, letterButtons[finalI][finalJ].getText().toLowerCase());

                });
                app.getGUI().getPrimaryScene().setOnMouseDragReleased(e->{

                    gameplayController.handleWordSelection(lettersGuessed);
                });
                letterButtons[i][j].setOnMouseDragReleased(e->{
                    gameplayController.handleWordSelection(lettersGuessed);

                });

            }
        }
    }



    @Override
    public void initStyle() {

    }

    @Override
    public void reloadWorkspace() {

    }

    @Override
    /*
    Purpose is for when game is restarting from the gameplay screen
    reinitializes letter grid, score display, target score
     */
    public void reinitialize() {
        DataManager dm=(DataManager) app.getDataComponent();
        if(dm.getCurrentProfile().isHighestLevel(dm.getCurrentLevel()))
             resetData(false);
        else
            resetData(true);
        initLetters();
        initializeTargetScore(dm);


    }

    /**
     * Resets the data displayed in the UI
     */
    public void resetData(boolean val){
        scoresVBox.getChildren().clear();
        wordsVBox.getChildren().clear();
        currentScore.setText(""+0);
        lettersGuessed.setText("");
        targetScore.setText("");
        nextLevel.setVisible(val);
    }

    @Override
    public void customizeToolbar() {
        DataManager dm=(DataManager) app.getDataComponent();
        usernameButton.setText(dm.getCurrentProfile().getUsername()); //dummy place holder
        usernameButton.setPrefWidth(200);
        homeButton.setText("Home");
        homeButton.setPrefWidth(200);
        if(gui.getToolbarPane().getChildren()!=null)
            gui.getToolbarPane().getChildren().clear();
        gui.getToolbarPane().getChildren().addAll(usernameButton, homeButton);

    }
    private void initToolbarButtons(){
        usernameButton=new Button();
        homeButton=new Button();
    }

    /**
     * Highlights button corresponding to coordinates in grid
     * x and y must be be
     */
    public void highlightButton(int x,int y){
        reentrantlock=new ReentrantLock();
        reentrantlock.lock();
        try{
            PropertyManager pm=PropertyManager.getManager();
            if(x>=0 && y>=0&& x<LETTER_GRID_HEIGHT&& y<LETTER_GRID_WIDTH){
                DropShadow glow=new DropShadow();
                glow.setRadius(10);
                glow.setSpread(.5);
                glow.setColor(Color.YELLOW);

                letterButtons[x][y].setEffect(glow);
            }
        }finally{
            reentrantlock.unlock();
        }


    }

    /**
     * Updates the score display visual to reflect the word just selected
     * @param word the word selected
     * @param wordScore score of the word selected
     * @param cumScore cumulative score
     */
    public void updateScoreDisplay(String word, int wordScore, int cumScore){
        Text text=new Text(word);
        text.setFill(Color.GREY);
        text.setStyle("-fx-font-size: 12pt;");
        wordsVBox.getChildren().add(text);
        Text wScore;
        if(wordScore<10)
            wScore=new Text(" "+wordScore);
        else
            wScore=new Text(""+wordScore);
        wScore.setFill(Color.GREY);
        wScore.setStyle("-fx-font-size: 12pt;");
        scoresVBox.getChildren().add(wScore);

        currentScore.setText(""+cumScore);
    }

    /**
     * pre: Condition of adjacent coordinates is met
     *  coordinates are button coordinates in letterButtons.
     *  Must be translated to coordinates in letterGrid
     *
     * @return line highlighted
     */
    public Node highlightLine(int x1, int y1, int x2, int y2){
        reentrantlock=new ReentrantLock();
        reentrantlock.lock();
        try{
            PropertyManager pm=PropertyManager.getManager();
            //buttons in the letter grid are at coordinates *2 that of their coordinates in letterButtons
            x1*=2;
            y1*=2;
            x2*=2;
            y2*=2;
            //coordinates of line between the two buttons is the average of xs and average of ys
            int lineX=(x1+x2)/2;
            int lineY=(y1+y2)/2;
            for(Node n: letterGrid.getChildren()) {
                if (letterGrid.getRowIndex(n) == lineX && letterGrid.getColumnIndex(n) == lineY) {
                    DropShadow ds=new DropShadow();
                    ds.setColor(Color.YELLOW);
                    ds.setRadius(10);
                    ds.setSpread(.4);
                    n.setEffect(ds);
                    //n.getStyleClass().setAll(pm.getPropertyValue(HIGHLIGHT_CSS));
                    return n;
                }
            }
            return null;
        }finally{
            reentrantlock.unlock();
        }
    }

    public ArrayList selectAllOccurences(char letter){
        ArrayList paths=new ArrayList();
        char uppercaseLetter=Character.toUpperCase(letter);
        for(int i=0;i<letters.length;i++){
            for( int j=0;j<letters[0].length;j++){
                if(letters[i][j]==uppercaseLetter){
                    highlightButton(i,j);
                    TreeItem<Button> root=new TreeItem<Button> (letterButtons[i][j]);
                    TreeView<Button> tree=new TreeView<Button> (root);
                    paths.add(tree);
                }
            }
        }
        return paths;
    }

    public HashSet<TreeItem<Button>> addAllOccurences(char letter){
        HashSet<TreeItem<Button>> roots=new HashSet<>();
        char uppercaseLetter=Character.toUpperCase(letter);
        for(int i=0;i<letters.length;i++){
            for( int j=0;j<letters[0].length;j++){
                if(letters[i][j]==uppercaseLetter){
                    highlightButton(i,j);
                    TreeItem<Button> root=new TreeItem<Button> (letterButtons[i][j]);
                    TreeView<Button> tree=new TreeView<Button> (root);
                    roots.add(root);
                }
            }
        }
        return roots;

    }


    public Button[][] getLetterButtons(){return letterButtons;}

    public void unHighlightButton(int x,int y){
        letterButtons[x][y].setEffect(null);
    }
    public void unHighlightAllButtons(){
        for(int i=0;i<letterButtons.length;i++)
        {
            for(int j=0;j<letterButtons.length;j++){
                letterButtons[i][j].setEffect(null);
            }

        }
    }

    public void showEndGraphics(boolean isPersonalBest, boolean levelCleared, HashSet<String> wordsNotSelected, int maxScore){
        PropertyManager pm=PropertyManager.getManager();
        playPause.setGraphic(playImage);
        showWordSolution(wordsNotSelected);
        if(!nextLevel.isVisible())
             nextLevel.setVisible(levelCleared);
        if(isPersonalBest){
            AppMessageDialogSingleton dialog=AppMessageDialogSingleton.getSingleton();
            dialog.show(pm.getPropertyValue(GAME_OVER_TITLE), pm.getPropertyValue(PERSONAL_BEST_MESSAGE));
        }

    }

    private void showWordSolution(HashSet<String> words){
        for(String word: words){
            Text text=new Text(word);
            text.setFill(Color.RED);
            text.setStyle("-fx-font-size: 12pt;");
            wordsVBox.getChildren().add(text);
        }
    }

    public void setNextLevelVisibility(boolean val){
        nextLevel.setVisible(val);
    }

}
