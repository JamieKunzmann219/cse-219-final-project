package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HomeController;
import controller.MainController;
import data.DataManager;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;
import java.util.ArrayList;

import static buzzword.BuzzwordProperties.*;

/**
 * Created by jamie on 11/7/2016.
 * @author Jamie Kunzmann
 */
public class HomeScreen extends Workspace{


   private AppTemplate app; // the actual application
   private AppGUI gui; // the GUI inside which the application sits

    private GridPane logo;

    private BorderPane homeWorkspace; //contains all of the components of the Home Screen
    private HomeController homeController; //controller for this ui
    private MainController mainController;

    //toolbar options
    private Button createProfileButton; //button to create profile
    private Button loginButton;
    private Button helpButton;
    private ComboBox gameModeMenu;
    private  String[] modeOptions={"English Dictionary", "Names", "Verbs"};
    private Button account;
    private Button startPlaying;

    //login screen
    private GridPane createProfile;
    private GridPane login;
    private Label profileNameLbl;
    private Label passwordLbl;
    private Label reenterPasswordLbl;
    private TextField usernameTxt;
    private PasswordField passwordTxt;
    private PasswordField reenteredPasswordTxt;
    private Scene loginScene;
    private Stage loginStage;
    private Scene createProfileScene;
    private Stage createProfileStage;
    private Button loginComplete;
    private Button createProfileComplete;


    private HBox errorFeedback;
    private VBox newProfile;
    private Text feedbackText;

    private HBox loginFeedback;
    private  VBox loginVBox;
    private Text loginFeedbackText;

    private final KeyCodeCombination loginKC= new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN);
    private final KeyCodeCombination createProfileKC= new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN);


    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public HomeScreen(AppTemplate initApp) {
        app = initApp;
        gui = app.getGUI();
        mainController=(MainController)gui.getFileController();
        homeController=mainController.getHomeController();
        homeWorkspace=new BorderPane();
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        initializeToolbarButtons();
        initializeCreateProfileScreen();
        initializeLoginScreen();
        setupHandlers(); // ... and set up event handling

    }

    public void displayModeToolbar(String username){
        account.setText(username);
        if(gui.getToolbarPane().getChildren()!=null)
            gui.getToolbarPane().getChildren().clear();
        gui.getToolbarPane().getChildren().addAll(account, gameModeMenu, startPlaying, helpButton);


    }

    private void initializeToolbarButtons(){
        createProfileButton=new Button("Create New Profile");
        loginButton=new Button("Log In");
        gameModeMenu=new ComboBox();
        gameModeMenu.setPromptText("Game Mode");
        gameModeMenu.getItems().addAll(modeOptions);
        account=new Button();
        startPlaying=new Button("Start Playing");
        helpButton=new Button("Help");
        helpButton.setPrefWidth(200);
        createProfileButton.setPrefWidth(200);
        loginButton.setPrefWidth(200);
        gameModeMenu.setPrefWidth(200);
        account.setPrefWidth(200);
        startPlaying.setPrefWidth(200);

    }

    public BorderPane getWorkspace(){
        return homeWorkspace;
    }


    /**
     * screen state is HOME_LOGIN_SCREEN or HOME_MODE_SCREEN
     */
    public void customizeToolbar(ScreenState state)
    {
        DataManager dm= (DataManager) app.getDataComponent();
        if(state==ScreenState.HOME_LOGIN_SCREEN)
            displayLoginToolbar();
        else if(state==ScreenState.HOME_MODE_SCREEN)
            displayModeToolbar(dm.getCurrentProfile().getUsername());

    }
    public void displayLoginToolbar(){
        if (gui.getToolbarPane().getChildren() != null)
            gui.getToolbarPane().getChildren().clear();
        gui.getToolbarPane().getChildren().addAll(createProfileButton, loginButton, helpButton);
    }

    private void initializeLoginScreen(){

        profileNameLbl=new Label("Profile Name: ");
        passwordLbl=new Label("Password: ");

        passwordTxt=new PasswordField();
        usernameTxt=new TextField();
        loginComplete=new Button("LOG IN");
        loginComplete.setStyle("-fx-background-color: #57FF5C;");

        login=new GridPane();
        login.setHgap(10);
        login.setVgap(20);
        login.getChildren().clear();
        login.add(profileNameLbl, 0,0);
        login.add(usernameTxt, 1,0);
        login.add(passwordLbl, 0,1);
        login.add(passwordTxt, 1,1);
        login.add(loginComplete,1,2);
        login.setStyle("-fx-background-color: #5E5E5E;");
        login.setPadding(new Insets(30,20,5,20));

        loginVBox=new VBox();
        loginFeedback=new HBox();
        loginFeedback.setMinHeight(15);
        loginFeedback.setPadding(new Insets(0,5,5,5));
        loginFeedback.setAlignment(Pos.CENTER);
        loginFeedback.setStyle("-fx-background-color: #5E5E5E;");
        loginVBox.getChildren().addAll(login,loginFeedback);


        loginScene=new Scene(loginVBox);

    }
    private void initializeCreateProfileScreen() {
        createProfile = new GridPane();
        profileNameLbl = new Label("Profile Name: ");
        passwordLbl = new Label("Password: ");
        reenterPasswordLbl = new Label("Confirm Password: ");
        passwordTxt = new PasswordField();
        usernameTxt = new TextField();
        reenteredPasswordTxt = new PasswordField();
        createProfileComplete = new Button("Create Profile");
        createProfileComplete.setStyle("-fx-background-color: #57FF5C;");

        createProfile.setHgap(10);
        createProfile.setVgap(20);
        createProfile.getChildren().clear();
        createProfile.add(profileNameLbl, 0, 0);
        createProfile.add(usernameTxt, 1, 0);
        createProfile.add(passwordLbl, 0, 1);
        createProfile.add(passwordTxt, 1, 1);
        createProfile.add(reenterPasswordLbl, 0, 2);
        createProfile.add(reenteredPasswordTxt, 1, 2);
        createProfile.add(createProfileComplete, 1, 3);
        createProfile.setStyle("-fx-background-color: #5E5E5E;");
        createProfile.setPadding(new Insets(30, 20, 5, 20));
        newProfile=new VBox();
        errorFeedback=new HBox();
        errorFeedback.setMinHeight(15);
        errorFeedback.setPadding(new Insets(0,5,5,5));
        errorFeedback.setAlignment(Pos.CENTER);
        errorFeedback.setStyle("-fx-background-color: #5E5E5E;");
        newProfile.getChildren().addAll(createProfile,errorFeedback);

        createProfileScene = new Scene(newProfile);
    }


    public void initCreateProfileStage(){
        createProfileStage=new Stage();
        createProfileStage.setScene(createProfileScene);
        createProfileStage.initModality(Modality.WINDOW_MODAL);
        createProfileStage.initOwner(app.getGUI().getWindow());
        //createProfileStage.initStyle(StageStyle.TRANSPARENT);
    }
    public void initLoginStage() {
        loginStage = new Stage();
        loginStage.setScene(loginScene);
        loginStage.initModality(Modality.WINDOW_MODAL);
        loginStage.initOwner(app.getGUI().getWindow());
        // loginStage.initStyle(StageStyle.TRANSPARENT);}

    }


    private void layoutGUI() {

        Button b;
        logo=new GridPane();
        logo.setHgap(15);
        logo.setVgap(15);
        for(int i=0;i<4;i++)
        {
            for(int j=0;j<4;j++)
            {
                if(i==0&&j==0) {
                    b = new Button("B");

                }
                else if(i==0&&j==1) {
                    b = new Button("Z");

                }
                else if(i==1&&j==0) {
                    b = new Button("U");

                }
                else if(i==1&&j==1) {
                    b = new Button("Z");

                }
                else if(i==2&&j==2) {
                    b = new Button("W");

                }
                else if(i==2&&j==3) {
                    b = new Button("R");

                }
                else if(i==3&&j==2) {
                    b = new Button("O");

                }
                else if(i==3&&j==3) {
                    b = new Button("D");

                }
                else b=new Button();
                //b.setDisable(true);
                b.setStyle("-fx-background-color: #A4F3F5;"+"-fx-font-size: 14pt;"+"-fx-font-family: Algerian;"+"-fx-text-fill: black;");
                b.setShape(new Circle(25));
                b.setPrefWidth(50);
                b.setPrefHeight(50);
                logo.add(b, i,j);

            }
        }
        logo.setAlignment(Pos.CENTER);
        homeWorkspace.setCenter(logo);

    }

    private void setupHandlers()
    {
        loginButton.setOnMouseClicked(e -> homeController.handleLoginRequest());
        createProfileButton.setOnMouseClicked(e->homeController.handleCreateProfileRequest());
        loginComplete.setOnAction(e -> homeController.handleLoginCompletion());
        createProfileComplete.setOnAction(e -> homeController.handleCreateProfileCompletion());
        startPlaying.setOnAction(e-> {
            if(gameModeMenu.getValue()==null)
                homeController.handleModeSelection("");
            else
                homeController.handleModeSelection(gameModeMenu.getValue().toString());
            } );
        account.setOnAction(e->mainController.handleAccountRequest());
        helpButton.setOnAction(e->homeController.handleHelpRequest());
        app.getGUI().getPrimaryScene().addEventHandler(KeyEvent.KEY_RELEASED,(KeyEvent event) -> {
            if(createProfileKC.match(event))
                homeController.handleCreateProfileRequest();
            else if(loginKC.match(event))
                homeController.handleLoginRequest();
        });




    }
    public ComboBox getGameModeMenu(){return gameModeMenu;}

    public void createProfileFeedback(String feedback){

        errorFeedback.getChildren().clear();
        feedbackText=new Text();
        feedbackText.setText(feedback);
        feedbackText.setFill(Color.RED);

        errorFeedback.getChildren().add(feedbackText);
    }
    public void loginFeedback(String feedback){
        loginFeedback.getChildren().clear();
        loginFeedbackText=new Text();
        loginFeedbackText.setText(feedback);
        loginFeedbackText.setFill(Color.RED);
        loginFeedback.getChildren().add(loginFeedbackText);
    }

    public void clearLoginMessage(){
        loginFeedback.getChildren().clear();
    }
    public void clearCreateErrorMessage(){
        errorFeedback.getChildren().clear();
    }

    public Stage getLoginStage(){return loginStage;}
    public Stage getCreateProfileStage(){return createProfileStage;}
    public GridPane getCreateProfileGrid(){return createProfile;}
    public GridPane getLoginGrid(){return login;}
    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {

        //homeHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

}
