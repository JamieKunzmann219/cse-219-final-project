package gui;

import apptemplate.AppTemplate;
import controller.MainController;
import data.DataManager;
import data.Mode;
import data.Profile;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ui.AppGUI;

import java.util.HashMap;

/**
 * Created by jamie on 12/10/2016.
 * @author Jamie Kunzmann
 */
public class ProfileSettingsScreen extends Stage {
    AppTemplate app;
    Button saveButton;
    Button logoutButton;
    VBox statsContainer;
    HBox statsHBox;
    VBox profileVBox ;
    Scene profileScene;
    HBox currentUsernameHBox;
    HBox currentPasswordHBox;
    VBox profileContainer;
    HBox editPasswordHBox;
    PasswordField newPassword;
    Text username;
    Text password;
    MainController mainController;
    HBox buttonHBox;

    private final String[] modes={"ENGLISH_DICTIONARY", "NAMES", "VERBS"};
    private final String [] lowercaseMode={"English Dictionary", "Names", "Verbs"};
    public ProfileSettingsScreen(AppTemplate app){
        layoutScreen();
        this.app=app;
        initModality(Modality.WINDOW_MODAL);
        initOwner(app.getGUI().getWindow());
        setUpHandlers();
        AppGUI gui=app.getGUI();
        mainController=(MainController)gui.getFileController();
    }
    private void setUpHandlers(){
        saveButton.setOnAction(e->mainController.handleProfileEdited(newPassword,this));
        logoutButton.setOnAction(e->mainController.handleLogoutRequest(this));
    }


    private void layoutScreen(){
        //title
        Label title= new Label("Profile Settings");
        title.setStyle("-fx-underline: true;"+ "-fx-text-fill:#57FF5C;"+"-fx-font-size: 14pt;");
        title.setAlignment(Pos.CENTER);

        //username/ password
        currentUsernameHBox=new HBox();
        //currentPasswordHBox.setSpacing(5);
        editPasswordHBox=new HBox();
        editPasswordHBox.setSpacing(5);
        Label usernameLabel=new Label ("Profile Name: ");
        usernameLabel.setTextFill(Color.WHITE);
        usernameLabel.setTextFill(Color.WHITE);
        Label editLabel=new Label("Change Password: ");
        editLabel.setTextFill(Color.WHITE);
        username=new Text();
        username.setFill(Color.WHITE);
        newPassword=new PasswordField();
        editPasswordHBox.getChildren().addAll(editLabel, newPassword);
        currentUsernameHBox.getChildren().addAll(usernameLabel, username);


        profileContainer=new VBox();
        profileContainer.setSpacing(5);
        profileContainer.getChildren().addAll(currentUsernameHBox, editPasswordHBox);


        //stats
        statsHBox=new HBox();
        statsHBox.setSpacing(10);
        Label scoreLabel=new Label("Personal Bests");
        scoreLabel.setStyle("-fx-underline: true;"+ "-fx-text-fill: #ffffff;"+"-fx-font-size: 12pt;");
        scoreLabel.setAlignment(Pos.CENTER);
        statsContainer=new VBox();
        statsContainer.setAlignment(Pos.CENTER);
        statsContainer.setSpacing(10);
        statsContainer.getChildren().addAll(scoreLabel, statsHBox);
        //layoutEdit();
        saveButton=new Button("Save/Close");
        saveButton.setStyle("-fx-background-color: #57FF5C;");

        logoutButton=new Button("Logout");
        logoutButton.setStyle("-fx-background-color: #57FF5C;");
        buttonHBox=new HBox();
        buttonHBox.setAlignment(Pos.CENTER);
        buttonHBox.setSpacing(10);
        buttonHBox.getChildren().addAll(saveButton, logoutButton);


        profileVBox=new VBox();
        profileVBox.setStyle("-fx-background-color: #5E5E5E;");
        profileVBox.setSpacing(20);
        profileVBox.setAlignment(Pos.CENTER);
        profileVBox.setPadding(new Insets(10,10,10,10));
        profileVBox.getChildren().addAll(title,profileContainer, statsContainer, buttonHBox);


        profileScene=new Scene(profileVBox);
        this.setScene(profileScene);

        this.initStyle(StageStyle.TRANSPARENT);
        this.setTitle("Profile Settings");

    }

    public void showScreen(){
        this.show();
    }
    public void reset(){
        newPassword.setText("");
        statsHBox.getChildren().clear();
    }



    public void setStats(Profile p){
        username.setText(p.getUsername());


       HashMap<String, HashMap<Integer,Integer>> scores= p.getAllHighScores();
        for(int i=0;i<modes.length;i++ ){
            HashMap<Integer, Integer> levelScores=scores.get(modes[i]);
            VBox modeData=new VBox();
            modeData.setSpacing(5);

            Label l=new Label(lowercaseMode[i]);
            l.setAlignment(Pos.CENTER);
            l.setStyle("-fx-text-fill: #57FF5C;"+"-fx-underline: true;");
            modeData.getChildren().add(l);
            for(Integer j=1;j<=levelScores.size();j++){
                Text t=new Text("Level "+j.toString()+":  "+ levelScores.get(j));
                t.setFill(Color.WHITE);
                modeData.getChildren().add(t);
            }
            statsHBox.getChildren().add(modeData);
        }


    }






}
