package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HomeController;
import controller.LevelController;
import controller.MainController;
import data.DataManager;
import data.Mode;
import data.Profile;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static buzzword.BuzzwordProperties.*;

/**
 * Created by jamie on 11/7/2016.
 * @author Jamie Kunzmann
 */
public class LevelScreen extends Workspace {
    AppTemplate app; // the actual application
    AppGUI gui; // the GUI inside which the application sits

    GridPane allGuesses;        //text area displaying all guesses
    GridPane          correctGuesses;    //text display of each correct guess
    GridPane levels;

    BorderPane levelWorkspace; //contains all of the components of the Home Screen
    VBox levelSelect;
    MainController mainController;
    LevelController levelController; //controller for level ui

    Button usernameButton;
    Button homeButton; //button to return to homescreen

    Label gameMode;

    ArrayList<Button> levelButtons;

    static Map<String,String> modeNames = new HashMap<String, String>();
    static {
        modeNames.put("ENGLISH_DICTIONARY","English Dictionary");
        modeNames.put("NAMES","Names");
        modeNames.put("VERBS","Verbs");

    }

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public LevelScreen(AppTemplate initApp) {
        app = initApp;
        gui = app.getGUI();
        mainController=(MainController)gui.getFileController();
        levelController=mainController.getLevelController();
        levelWorkspace=new BorderPane();
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout

    }


    public Pane getWorkspace(){
        return levelWorkspace;
    }



    public void customizeToolbar()
    {
        DataManager dm=(DataManager) app.getDataComponent();
        usernameButton=new Button(dm.getCurrentProfile().getUsername()); //dummy place holder
        homeButton=new Button("Home");
        usernameButton.setPrefWidth(200);
        homeButton.setPrefWidth(200);
        if(gui.getToolbarPane().getChildren()!=null)
            gui.getToolbarPane().getChildren().clear();
        gui.getToolbarPane().getChildren().addAll(usernameButton, homeButton);
    }

    private void layoutGUI() {

        levelSelect=new VBox();
        levelSelect.setSpacing(20);

        PropertyManager propertyManager=PropertyManager.getManager();

        gameMode =new Label("People"); //dummy placeholder
        gameMode.getStyleClass().setAll(propertyManager.getPropertyValue(GAME_MODE_LABEL_CSS));


        levels=new GridPane();
        levelSelect.getChildren().addAll(gameMode, levels);
        levelSelect.setAlignment(Pos.CENTER);
        levelWorkspace.setCenter(levelSelect);
    }
    public Pane getPlaces(){
        gameMode.setText("Verbs");
        initLevelsGrid(8,4);
        return levelWorkspace;
    }

    public void initScreen(){
        DataManager dm=(DataManager)app.getDataComponent();
        gameMode.setText(modeNames.get(dm.getCurrentProfile().getCurrentMode()));
        Profile p=dm.getCurrentProfile();
        HashMap<String, Mode> modes=dm.getModes();
        Mode mode=modes.get(p.getCurrentMode());
        int numTotalLevels=mode.getNumLevels();
        HashMap<String, Integer> numLevelsCompleteMap=p.getNumLevelsCompleted();
        int numComplete=numLevelsCompleteMap.get(p.getCurrentMode());
        initLevelsGrid(numTotalLevels, numComplete);
        setupHandlers();
    }
    public Pane getScreen(){return levelWorkspace;}



    private void initLevelsGrid(int size, int levelsCompleted) {
        int levelsUnlocked=1;
        if(levelsCompleted<size){
            levelsUnlocked=levelsCompleted+1;
        }else {
            levelsUnlocked=size;
        }

        levelButtons=new ArrayList<Button>();
        Button b;
        levels.getChildren().clear();
        levels.setHgap(10);
        levels.setVgap(10);

        levels.setAlignment(Pos.CENTER);

        int rowLength=size/2;
        for (int i = 1; i <= size; i++) {
            b=new Button();
            levelButtons.add(i-1,b);
            b.setPrefSize(50,50);
            b.setText(""+i);
            b.setShape(new Circle(25));

            levels.add(b,(i-1)%(rowLength), (i-1)/(rowLength));
            if(i>levelsUnlocked){
                levels.getChildren().get(i-1).setDisable(true);
            }

        }
    }

    private void setupHandlers()
    {
        DataManager dm=(DataManager) app.getDataComponent();
        int numLevels=dm.getModes().get(dm.getCurrentProfile().getCurrentMode()).getNumLevels();
        for(int i=0;i<numLevels;i++){
            int finalI = i;
            levelButtons.get(i).setOnAction(e -> {levelController.handleLevelSelect(finalI +1);});

        }
        usernameButton.setOnAction(e->mainController.handleAccountRequest());
        homeButton.setOnAction(e->levelController.handleHomeRequest());




    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {

       // guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

}
