package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.GameplayController;
import controller.MainController;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;
import java.util.ArrayList;

import static buzzword.BuzzwordProperties.*;
import static gui.Workspace.ScreenState.LEVEL_SCREEN;
import static java.awt.event.KeyEvent.VK_ENTER;
import static javafx.scene.input.KeyEvent.*;


/**
 * Created by jamie on 11/7/2016.
 * @author Jamie Kunzmann
 */
public class Workspace extends AppWorkspaceComponent{
    public Workspace() {
    }

    public static final String HOME_LOGIN="HOME_LOGIN_SCREEN";
    public static final String HOME_MODE="HOME_MODE_SCREEN";
    public static final String GAMEPLAY="GAMEPLAY_SCREEN";
    public static final String LEVEL="LEVEL_SCREEN";



    public enum ScreenState{
        HOME_LOGIN_SCREEN,
        HOME_MODE_SCREEN,
        GAMEPLAY_SCREEN,
        LEVEL_SCREEN
    }

    ScreenState screenstate; //current screen that is displayed to the user
    AppTemplate app; // the actual application
    AppGUI gui; // the GUI inside which the application sits
    MainController controller;
    GameplayScreen gameplayScreen; //scene for gameplay screen
    HomeScreen homeScreen; //scene for home screen
    LevelScreen levelScreen; //scene for level screen
    ProfileSettingsScreen profileSettingsScreen;
    HelpScreen helpScreen;

    Label guiHeadingLabel;
    HBox headPane;

    BorderPane workspaceContainer;
    private final KeyCodeCombination replayKC= new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN);



    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();


        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        workspace=workspaceContainer;

        controller = (MainController) gui.getFileController();
        homeScreen=new HomeScreen(app);
        gameplayScreen=new GameplayScreen(app);
        levelScreen=new LevelScreen(app);
        profileSettingsScreen=new ProfileSettingsScreen(app);
        helpScreen=new HelpScreen(app);
        screenstate=ScreenState.HOME_LOGIN_SCREEN;
        setToolbar(homeScreen);
        workspaceContainer.setCenter(homeScreen.getWorkspace());

        setHandlers();



    }
    public ProfileSettingsScreen getProfileSettingsScreen(){return profileSettingsScreen;}

    public HelpScreen getHelpScreen(){return helpScreen;}
    public void screenTransition(String screen) {
        if(screen.equals(LEVEL)){
            screenstate= LEVEL_SCREEN;
            setToolbar(levelScreen);
            levelScreen.initScreen();

            workspaceContainer.setCenter(levelScreen.getScreen());

        }else if (screen.equals(HOME_MODE)) {
            screenstate = ScreenState.HOME_MODE_SCREEN;
            setToolbar(homeScreen);
            workspaceContainer.setCenter(homeScreen.getWorkspace());
        }
        else if(screen.equals(GAMEPLAY)) {
            screenstate = ScreenState.GAMEPLAY_SCREEN;
            setToolbar(gameplayScreen);
            gameplayScreen.initScreen();
            workspaceContainer.setCenter(gameplayScreen.getScreen());
        }else if(screen.equals(HOME_LOGIN)){
            screenstate=ScreenState.HOME_LOGIN_SCREEN;
            setToolbar(homeScreen);
            workspaceContainer.setCenter(homeScreen.getWorkspace());
        }




    }
    public HomeScreen getHomeScreen(){return homeScreen;}
    public LevelScreen getLevelScreen(){return levelScreen;}
    public GameplayScreen getGameplayScreen(){return gameplayScreen;}
    public void setToolbar(AppWorkspaceComponent screen){

        if(screen==homeScreen){
            homeScreen.customizeToolbar(screenstate);
        }else
            screen.customizeToolbar();
    }

    private void setHandlers(){
        GameplayController gc= controller.getGameplayController();
        app.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
            if(!event.isControlDown()){
                char selection = event.getCharacter().charAt(0);
                if (Character.isLetter(selection)) {
                    char uppercaseSelection=Character.toUpperCase(selection);
                    gc.handleLetterTyped(uppercaseSelection);
                }
            }
        });
        app.getGUI().getPrimaryScene().setOnKeyPressed((KeyEvent e)->{
            if(replayKC.match(e))
                gc.handleReplayRequest(gameplayScreen);
        });


        app.getGUI().getPrimaryScene().setOnKeyReleased((KeyEvent e)-> {
                    if (e.getCode() == KeyCode.ENTER)
                        gc.handleWordSelection(gameplayScreen.lettersGuessed);
                });

    }




    public void setScreenState(ScreenState screenState){this.screenstate=screenState;}
    public ScreenState getScreenState(){return screenstate;}


    private void layoutGUI() {
        PropertyManager propertyManager= PropertyManager.getManager();

        workspaceContainer=new BorderPane();


        headPane=new HBox();
        headPane.setAlignment(Pos.CENTER);
        guiHeadingLabel=new Label(propertyManager.getPropertyValue(HOME_HEADING_LABEL));

        headPane.getChildren().add(guiHeadingLabel);
        workspaceContainer.setTop(headPane);


    }


    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        gui.getToolbarPane().setAlignment(Pos.CENTER);
        gui.getToolbarPane().setVgap(20);
        gui.getToolbarPane().setMinWidth(240);

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        //Label headingLabel=homeScreen.getLabel();

        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HOME_HEADING_LABEL_CSS));
    }

    @Override
    public void reloadWorkspace() {

    }

    @Override
    public void reinitialize() {

    }

    @Override
    public void customizeToolbar() {

    }
}
