package gui;

import apptemplate.AppTemplate;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created by jamie on 12/10/2016.
 */
public class HelpScreen extends Stage {
    AppTemplate app;
    private Scene helpScene;
    private ScrollPane scrollContainer;
    private VBox container;
    private Label title;
    private Label introLabel;
    private Label modeLabel;
    private Label gameplayLabel;
    private Label scoringLabel;
    private final String titleString="Help";
    private final String introString="Overview";
    private final String modeString="Modes";
    private final String gameplayString="Game Play";
    private final String scoringString="Scoring";
    private Label introText;
    private Label modeText;
    private Label gameText;
    private Label scoreText;
    private final String intro="BuzzWord is a word game based off of the popular game Boggle. " +
            "Players select words in a grid to obtain points. The goal is to reach the target score" +
            " before the time runs out.";
    private final String modes="In BuzzWord, players can choose from three different modes: " +
            "English Dictionary, Names, and Verbs. Only words that are associated with the selected mode" +
            " are valid to select during game play. Additionally, words selected must have at least three" +
            " letters. \n\nEach mode has six levels. Initially, only the first level of each mode is unlocked." +
            "When a player clears a level (by reaching the target score), the next level is unlocked.";
    private final String gameplay="The BuzzWord board consists of a 4x4 grid of circles that each contain a letter. " +
            "To select a word, players" + " can either mouse drag over the circles or type letters that appear in the" +
            " grid. Upon releasing drag or pressing the enter key, it is then determined whether or not the letters form a valid word. " +
            "Valid words must meet three requirements. 1. The letters must match a word in the mode's dictionary. 2. The adjacent letters in " +
            "the word must be adjacent in the grid. Letters are considered adjacent in the grid" +
            " if they are nearest neighbors vertically, horizontally, or diagonally. 3. Each circle can only be selected once per word."+
            "Once a word is selected, that same word cannot be selected again, even if it is found through a different path.";

    private final String scoring="The target score for each level is based off of a percentage of the maximum obtainable score for the current grid."
            +"The higher the level, the higher the percentage. The total score is calculated as the sum of the score for each word, " +
            "which varies by word length. Longer words are worth more points.";
    public HelpScreen (AppTemplate app){
        this.app=app;
        initModality(Modality.WINDOW_MODAL);
        initOwner(app.getGUI().getWindow());
        layoutScreen();
    }

    public void layoutScreen(){

        title=new Label(titleString);
        title.setStyle("-fx-underline: true;"+ "-fx-font-size: 16pt;"+ "-fx-text-fill: #ffffff;");

        introLabel=new Label(introString);
        introLabel.setStyle("-fx-underline: true;"+ "-fx-font-size: 14pt;"+ "-fx-text-fill: #ffffff;");
        modeLabel=new Label(modeString);
        modeLabel.setStyle("-fx-underline: true;"+ "-fx-font-size: 14pt;"+ "-fx-text-fill: #ffffff;");
        gameplayLabel=new Label(gameplayString);
        gameplayLabel.setStyle("-fx-underline: true;"+ "-fx-font-size: 14pt;"+ "-fx-text-fill: #ffffff;");
        scoringLabel=new Label(scoringString);
        scoringLabel.setStyle("-fx-underline: true;"+ "-fx-font-size: 14pt;"+ "-fx-text-fill: #ffffff;");

        introText=new Label(intro);
        introText.setWrapText(true);
        introText.setTextFill(Color.LIMEGREEN);

        modeText=new Label(modes);
        modeText.setWrapText(true);
        modeText.setTextFill(Color.LIMEGREEN);

        gameText=new Label(gameplay);
        gameText.setWrapText(true);
        gameText.setTextFill(Color.LIMEGREEN);

        scoreText=new Label(scoring);
        scoreText.setWrapText(true);
        scoreText.setTextFill(Color.LIMEGREEN);
        VBox i=new VBox();
        i.setSpacing(5);
        i.getChildren().addAll(introLabel, introText);
        VBox m=new VBox();
        m.setSpacing(5);
        m.getChildren().addAll(modeLabel, modeText);
        VBox g=new VBox();
        g.setSpacing(5);
        g.getChildren().addAll(gameplayLabel, gameText);
        VBox s=new VBox();
        s.setSpacing(5);
        s.getChildren().addAll(scoringLabel, scoreText);

        container=new VBox();
        container.setSpacing(20);
        container.setMaxWidth(300);
        container.getChildren().addAll(i, m, g, s);
        container.setStyle("-fx-background-color: #5E5E5E;");
        container.setPadding(new Insets(20,20,20,20));

        scrollContainer=new ScrollPane(container);
        scrollContainer.setMaxHeight(500);
        scrollContainer.setPrefWidth(300);
        scrollContainer.setMaxWidth(300);
        scrollContainer.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollContainer.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        helpScene=new Scene(scrollContainer);
        this.setScene(helpScene);
        this.setTitle(titleString);
    }

    public void showScreen(){
        this.showAndWait();
    }





}
