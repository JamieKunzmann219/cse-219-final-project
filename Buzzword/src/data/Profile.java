package data;

import propertymanager.PropertyManager;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by jamie on 11/7/2016.
 * @author Jamie Kunzmann
 */
public class Profile {

    String username;
    String password;
    String currentMode;
    HashMap<String,Integer> numLevelsCompleted;
    HashMap<String, HashMap<Integer,Integer>> allHighScores;



    public Profile(String username, String password, String[] modes){

        this.username=username;
        this.password=password;
        initializeNumLevelsCompleted(modes);
        initializeHighScores(modes);
        currentMode=modes[0];
    }
    public Profile(){

    }

    public void initializeNumLevelsCompleted(String[]modes){
        numLevelsCompleted=new HashMap<String, Integer>();
        for(int i=0;i<modes.length;i++)
        {
            numLevelsCompleted.put(modes[i], 0);
        }
    }

    public void initializeHighScores(String[] modes){
        PropertyManager pm=PropertyManager.getManager();
        allHighScores=new HashMap<String, HashMap<Integer, Integer>>();
        for(int i=0;i<modes.length;i++){
            allHighScores.put(modes[i], new HashMap<Integer, Integer>());


            int numLevels=Integer.parseInt(pm.getPropertyValue(modes[i]));
            for(int j=1;j<=numLevels;j++) {

                allHighScores.get(modes[i]).put(j,0);
            }

        }


    }

    public void updateNumLevelsCompleted(int numLevels){
        if(numLevels>numLevelsCompleted.get(currentMode))
             numLevelsCompleted.replace(currentMode, numLevels);
    }
    public boolean isHighestLevel(int currentLevel){
        return currentLevel>numLevelsCompleted.get(currentMode);
    }

    public void setNumLevelsCompleted(HashMap<String, Integer> map){numLevelsCompleted=map;}
    public HashMap<String, Integer> getNumLevelsCompleted(){return numLevelsCompleted;}

    public void setAllHighScores(HashMap<String,HashMap<Integer, Integer>> map){allHighScores=map;}
    public HashMap<String, HashMap<Integer, Integer>> getAllHighScores(){return allHighScores;}
    public String getUsername(){return username;}
    public void setUsername(String name){username=name;}
    public String getPassword(){return password;}
    public void setPassword(String p){password=p;}

    public void setCurrentMode(String m){
        if(m.equals("English Dictionary")){
            currentMode="ENGLISH_DICTIONARY";
        }else if(m.equals("Names")){
            currentMode="NAMES";
        }else if(m.equals("Verbs")){
            currentMode="VERBS";
        }else
            currentMode=m;
    }
    public String getCurrentMode(){return currentMode;}

    public boolean isPersonalBest(Mode mode, int level, int score){
       String modeKey= mode.getKey();
        HashMap<Integer, Integer> modeHighScores=allHighScores.get(modeKey);
        int oldScore=modeHighScores.get(level);
        if(score>oldScore){
            setHighScore(modeHighScores, level, score);
            return true;
        }
        return false;


    }
    public void setHighScore(HashMap<Integer,Integer> modeHighScores, int level, int newScore){
        modeHighScores.replace(level, newScore);
    }





}
