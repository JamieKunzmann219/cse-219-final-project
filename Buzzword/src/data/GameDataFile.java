package data;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Set;


/**
 * Created by jamie on 11/7/2016.
 * @author Jamie Kunzmann
 */
public class GameDataFile implements AppFileComponent{
    public static final String TARGET_WORD  = "TARGET_WORD";
    public static final String GOOD_GUESSES = "GOOD_GUESSES";
    public static final String BAD_GUESSES  = "BAD_GUESSES";
    public static final String HINT_AVAILABILITY= "HINT_AVAILABILITY";
    public static final String HINT_USED="HINT_USED";
    public static final String PROFILES="PROFILES";




    @Override
    public void saveData(AppDataComponent data, Path to) {
        DataManager allData = (DataManager) data;
        HashMap<String, Profile> profiles = allData.getProfiles();

        ObjectMapper objectMapper = new ObjectMapper();
        JsonFactory jsonFactory = new JsonFactory();

        try {

            objectMapper.writerWithDefaultPrettyPrinter().writeValue(new File(String.valueOf(to)), profiles);

        } catch (JsonGenerationException e) {

            e.printStackTrace();

        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void saveProfile(AppDataComponent data, Profile profile, Path to) {
        DataManager allData = (DataManager) data;
        HashMap<String, Profile> profiles = allData.getProfiles();

        ObjectMapper objectMapper = new ObjectMapper();
        JsonFactory jsonFactory = new JsonFactory();
        try {

            objectMapper.writerWithDefaultPrettyPrinter().writeValue(new File(String.valueOf(to)), profiles);

        } catch (JsonGenerationException e) {

            e.printStackTrace();

        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
/*
        try (OutputStream out = Files.newOutputStream(to)) {

            JsonGenerator generator = jsonFactory.createGenerator(out, JsonEncoding.UTF8);

            generator.writeStartObject();
            //objectMapper.write

           // Map<String,User> result = mapper.readValue(src, new TypeReference<Map<String,User>>() { });

            generator.writeFieldName(PROFILES);
            generator.writeStartArray(goodguesses.size());
            for (Character c : goodguesses)
                generator.writeString(c.toString());
            generator.writeEndArray();

            generator.writeFieldName(BAD_GUESSES);
            generator.writeStartArray(badguesses.size());
            for (Character c : badguesses)
                generator.writeString(c.toString());
            generator.writeEndArray();


            generator.writeEndObject();

            generator.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
 */

    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            DataManager allData=(DataManager) data;
        try {

            File jsonFile = new File(String.valueOf(from));
            if(jsonFile.length()!=0){
                HashMap<String, Profile> mapObject = mapper.readValue(jsonFile,

                        new TypeReference<HashMap<String, Profile>>() {
                        });
                allData.setProfiles(mapObject);
            }


        } catch (JsonGenerationException e) {

            e.printStackTrace();

        } catch (JsonMappingException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }



        }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }


    public void updateProfile(Profile p, Path path){
        try{
            byte[] jsonData = Files.readAllBytes(path);

            ObjectMapper objectMapper = new ObjectMapper();

//create JsonNode
            JsonNode rootNode = objectMapper.readTree(jsonData);

//update JSON data
            ((ObjectNode) rootNode).put("id", 500);
//add new key value
            ((ObjectNode) rootNode).put("test", "test value");
//remove existing key
            ((ObjectNode) rootNode).remove("role");
            ((ObjectNode) rootNode).remove("properties");
            objectMapper.writeValue(new File("updated_emp.txt"), rootNode);

        }catch(IOException e){
            System.out.println(e.getStackTrace());
        }


    }

}
