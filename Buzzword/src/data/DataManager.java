package data;


import apptemplate.AppTemplate;
import components.AppDataComponent;
import propertymanager.PropertyManager;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static buzzword.BuzzwordProperties.WORK_FILE;

/**
 * Created by jamie on 11/7/2016.
 * @author Jamie Kunzmann
 */
public class DataManager implements AppDataComponent{

    private static final int charConverter =65;
    private static final int NUM_ROWS =4;
    private static final int NUM_COLS=4;
    private HashMap<String, Profile> profiles;
    private Profile currentProfile;
    public  AppTemplate    appTemplate;
    private char[][] letterGridLetters;
    private static final String[] modeNames={"ENGLISH_DICTIONARY","NAMES",  "VERBS"};
    static Map<String, Integer> numLevels=new HashMap<String, Integer>();
    static {
        numLevels.put("ENGLISH_DICTIONARY",6);
        numLevels.put("NAMES",6);
        numLevels.put("VERBS",6);
    }
    private HashMap<String, Mode> modes;
    private static final double[] letterFreqCDF={8.167, 9.659, 12.441, 16.694, 29.396, 31.624, 33.639, 39.733, 46.699, 46.852, 47.624, 51.649, 54.055, 60.804, 68.311, 70.240, 70.335, 76.322, 82.649, 91.705, 94.463, 95.441, 97.801, 97.951, 99.926, 100.000};
    private EnglishDictionary englishDictionary;
    private Names names;
    private Verbs verbs;

    private HashSet<String> validWordsInGrid;

    private boolean[][]letterVisited;

    private int currentLevel;
    private Mode currentMode;
    private final double[] targetRatios={.10,.20, .30, .40, .50, .75};
    private int targetScore;

    private final int[] wordScore={0,0,0, 5,10,20,30,40,50,60,70,80,90,100,110,120,150};
    private int maxScore;

    public DataManager(AppTemplate appTemplate) {
        this(appTemplate, false);
    }

    public DataManager(AppTemplate appTemplate, boolean initiateGame) {
        if (initiateGame) {
            this.appTemplate = appTemplate;

        } else {
            this.appTemplate = appTemplate;
        }
        profiles=new HashMap<String, Profile>();

        modes=new HashMap<String, Mode>();
        modes.put("ENGLISH_DICTIONARY", new EnglishDictionary());
        modes.put("NAMES", new Names());
        modes.put("VERBS", new Verbs());



    }
    public int getMaxScore(){return maxScore;}

    public HashSet<String> getValidWordsInGrid(){return validWordsInGrid;}

    public void setCurrentProfile(Profile p){currentProfile=p;}

    public Profile getCurrentProfile(){return currentProfile;}

    public int getTargetScore(){return targetScore;}
    public int calculateTargetScore(){
        targetScore=0;
        maxScore=0;
        for(String word: validWordsInGrid){
            maxScore+=calculateWordScore(word);
        }
        targetScore= (int) (maxScore*targetRatios[currentLevel-1]);
        return targetScore;

    }
    public void loadData(){
        PropertyManager propertyManager= PropertyManager.getManager();
        Path path= Paths.get(propertyManager.getPropertyValue(WORK_FILE)).toAbsolutePath();
        try {
            appTemplate.getFileComponent().loadData(this,path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isExistingUsername(String username){
        return profiles.containsKey(username);
    }

    public void addProfile(Profile p){
        profiles.put(p.username, p);
    }
    public String[] getModeNames(){return modeNames;}
    public HashMap getProfiles(){return profiles;}
    public HashMap<String, Mode> getModes(){return modes;}
    public void setProfiles(HashMap<String, Profile> profilesMap){
        profiles=profilesMap;
    }

    public HashSet<String> findAllValidWords(){
        currentMode=modes.get(currentProfile.getCurrentMode());
        validWordsInGrid=new HashSet<String>();
        letterVisited=new boolean[letterGridLetters.length][letterGridLetters[0].length];

        //i, j represents coordinates of the starting point of the letter paths currently being searched
        for(int i=0;i<letterGridLetters.length;i++){
            for(int j=0;j<letterGridLetters.length;j++){
                recursivelyFindWords(letterGridLetters, validWordsInGrid, "", i, j);
            }
        }
        return validWordsInGrid;
    }

    public void recursivelyFindWords(char[][]letterGridLetters, HashSet<String> validWords, String letterPath, int i, int j)
    {
        letterPath+=Character.toString(letterGridLetters[i][j]).toLowerCase();
        letterVisited[i][j]=true;

        if(currentMode.isPrefix(letterPath)){
            //to be valid, must be at least 3 letters, in word list, and not already found
            if(letterPath.length()>2 && !validWords.contains(letterPath) && currentMode.containsWord(letterPath)){
                validWords.add(letterPath);
            }

            //checks path using each of its neighbors that have not been visited
            for(int r=i-1; r<=i+1; r++){
                for(int c=j-1; c<=j+1;c++){
                    //assures coordinates are in bounds
                    if(r>=0 && r<=letterGridLetters.length-1 && c>=0 && c<=letterGridLetters.length-1 ){
                        //only visits characters that have not already been considered for this path
                        if(!letterVisited[r][c]){
                            recursivelyFindWords(letterGridLetters, validWords, letterPath, r, c);
                        }
                    }
                }
            }
        }
        letterVisited[i][j]=false;

    }


    public char[][] generateLetterGrid(){
        letterGridLetters=new char[NUM_ROWS][NUM_COLS];
        for(int i=0;i<NUM_ROWS;i++){
            for(int j=0;j<NUM_COLS;j++){
                letterGridLetters[i][j]=generateRandomLetter();
            }
        }
        validWordsInGrid=findAllValidWords();
        return letterGridLetters;
    }

    public char generateRandomLetter(){
        double val=Math.random()*100;
        int low=0;
        int high=letterFreqCDF.length-1;

        if(val<letterFreqCDF[low]){
            return (char)(low+charConverter);
        }
        if(val>letterFreqCDF[high])
            return (char) (high+charConverter);
        //binary search for smallest cum frequency greater than val
        while(low<=high){
            int mid=(low+high)/2;
            if(val>letterFreqCDF[mid]){
                low=mid+1;
            }else if(val<letterFreqCDF[mid]) {
                high = mid - 1;
            }else return (char) (mid+ charConverter);
        }
        return (char) (low+charConverter); //low is index of smallest freq greater than val

    }
    public void setCurrentLevel(int level){currentLevel=level;}
    public int getCurrentLevel(){return currentLevel;}

    public int getNumValidWords(){
        return validWordsInGrid.size();
    }

    public void clearGameplayData(){
        validWordsInGrid=null;
        letterGridLetters=null;
        targetScore=0;
        maxScore=0;
    }

    public int calculateWordScore(String word){
        return wordScore[word.length()];
    }

    public boolean isPersonalBest(int score){
        return currentProfile.isPersonalBest(currentMode, currentLevel, score);
    }

    @Override
    public void reset() {

    }

}
