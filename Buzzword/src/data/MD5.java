package data;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by jamie on 12/2/2016.
 */
public class MD5 {


    public static String encryptPassword(String p){
        if(p.equals("")) return p;
        String encrypted="";
        try {
            MessageDigest md=MessageDigest.getInstance("MD5");
            byte[] bytes=p.getBytes();
            int length=p.length();
            md.update(bytes, 0, length);
            byte[] mdDigest=md.digest();
            BigInteger bi=new BigInteger(1,mdDigest);
            encrypted=bi.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return encrypted;
    }

}
