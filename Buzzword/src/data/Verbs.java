package data;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

/**
 * Created by jamie on 11/27/2016.
 * @author JamieKunzmann
 */
public class Verbs extends Mode {

    private ArrayList<String> words;
    private final String textFileName="verbs.txt";

    private final int numLevels=6;
    private final String lowercaseName="Verbs";
    private final String key="VERBS";

    public Verbs(){
        words=new ArrayList<String>();
        loadWords();
    }

    public String getKey(){return key;}
    public int getNumLevels(){return numLevels;}
    public String getLowercaseName(){return lowercaseName;}

    public void loadWords(){
        //  URL wordsResource = getClass().getClassLoader().getResource("words/words.txt");
        // assert wordsResource != null;
        ClassLoader classLoader = this.getClass().getClassLoader();
        File wordFile = null;
        try {
            wordFile = new File(classLoader.getResource("words/"+textFileName).toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        //File wordFile=new File("./"+textFileName);
        try {
            Scanner sc=new Scanner(wordFile);
            while(sc.hasNextLine()){
                words.add(sc.nextLine().toLowerCase());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean containsWord(String word){
        return super.containsWord(word, words);
    }
    public boolean isPrefix(String prefix){return super.isPrefix(prefix, words);}
    public ArrayList<String> getWordList(){return words;}
}
