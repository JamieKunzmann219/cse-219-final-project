package data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 * Created by jamie on 11/26/2016.
 */
public abstract class Mode {
    public abstract int getNumLevels();
    public abstract String getLowercaseName();
   abstract  ArrayList<String> getWordList();

    boolean isPrefix(String prefix, ArrayList<String> words){
        int index= Collections.binarySearch(words, prefix);

        //if prefix not a word in the list, check word following where it would be placed
        if(index<0){
            index=-index-1;
            //checks if this index is in bounce
            if(index==words.size()){
                return false;
            }
            if(words.get(index).startsWith(prefix))
                return true;
        }
        //case 2: prefix is a word in the list. then want to check the next word for the prefix
        if(index+1>words.size())
            return false;
        return words.get(index+1).startsWith(prefix);


    }
    abstract boolean isPrefix(String prefix);

    abstract boolean containsWord(String word);


    boolean containsWord(String word, ArrayList<String>words){
        int index=Collections.binarySearch(words, word);
        //returns number less than 0 if not in list
        if(index<0)
            return false;
        return true;
    }
    abstract String getKey();
}
