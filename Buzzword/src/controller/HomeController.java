package controller;

import data.MD5;
import apptemplate.AppTemplate;
//import com.sun.java.swing.plaf.windows.TMSchema;
import data.DataManager;
import data.GameDataFile;
import data.Profile;
import gui.HomeScreen;
import gui.Workspace;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import propertymanager.PropertyManager;
import sun.security.util.Password;
import ui.LoginSingleton;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static buzzword.BuzzwordProperties.*;

/**
 * Created by jamie on 11/7/2016.
 * @author Jamie Kunzmann
 */
public class HomeController extends MainController{

    AppTemplate appTemplate;
    HomeScreen homeScreen;
    Path workFile;


    public HomeController(AppTemplate appTemplate){
        this.appTemplate=appTemplate;
    }

    public void handleLoginRequest(){
        Workspace workspace=(Workspace)appTemplate.getWorkspaceComponent();
        if(workspace.getScreenState()== Workspace.ScreenState.HOME_LOGIN_SCREEN)
        {
            homeScreen=workspace.getHomeScreen();
            homeScreen.initLoginStage();
            homeScreen.getLoginStage().initModality(Modality.APPLICATION_MODAL);
            homeScreen.getLoginStage().showAndWait();
        }

    }
    public void handleCreateProfileRequest(){
        Workspace workspace=(Workspace)appTemplate.getWorkspaceComponent();
        if(workspace.getScreenState()== Workspace.ScreenState.HOME_LOGIN_SCREEN){
            homeScreen=workspace.getHomeScreen();
            homeScreen.initCreateProfileStage();
            homeScreen.getCreateProfileStage().initModality(Modality.APPLICATION_MODAL);
            homeScreen.getCreateProfileStage().showAndWait();
        }
    }
    public void handleCloseWindow(){
        Workspace workspace=(Workspace)appTemplate.getWorkspaceComponent();
        homeScreen=workspace.getHomeScreen();
        if(homeScreen.getCreateProfileStage()!=null && homeScreen.getCreateProfileStage().isShowing())
            homeScreen.getCreateProfileStage().close();
        else if(homeScreen.getLoginStage()!=null && homeScreen.getLoginStage().isShowing())
            homeScreen.getLoginStage().close();
    }

    public void handleLoginCompletion(){
        DataManager dataManager= (DataManager) appTemplate.getDataComponent();
        Workspace workspace= (Workspace) appTemplate.getWorkspaceComponent();
        homeScreen=workspace.getHomeScreen();
        TextField usernameTxt= (TextField) homeScreen.getLoginGrid().getChildren().get(1);
        PasswordField passwordTxt=(PasswordField) homeScreen.getLoginGrid().getChildren().get(3);
        String username=usernameTxt.getText();
        String password=passwordTxt.getText();
        if(!dataManager.isExistingUsername(username)){
            homeScreen.loginFeedback("Incorrect Profile Name");
        }else {
            Profile p = (Profile) dataManager.getProfiles().get(username);
            if (p != null && !p.getPassword().equals(MD5.encryptPassword(password))) {
                homeScreen.loginFeedback("Incorrect Password");
            }else
            if (p.getPassword().equals(MD5.encryptPassword(password))) {
                homeScreen.clearLoginMessage();
                dataManager.setCurrentProfile(p);
                homeScreen.getLoginStage().close();
                homeScreen.displayModeToolbar(username);
            }
        }
        usernameTxt.setText("");
        passwordTxt.setText("");
        workspace.setScreenState(Workspace.ScreenState.HOME_MODE_SCREEN);
    }

    public void handleCreateProfileCompletion(){
        PropertyManager propertyManager=PropertyManager.getManager();
        Workspace workspace=(Workspace)appTemplate.getWorkspaceComponent();
        DataManager dataManager=(DataManager) appTemplate.getDataComponent();
        TextField usernameText= (TextField) homeScreen.getCreateProfileGrid().getChildren().get(1);
        PasswordField passwordText= (PasswordField) homeScreen.getCreateProfileGrid().getChildren().get(3);
        PasswordField repasswordText=(PasswordField) homeScreen.getCreateProfileGrid().getChildren().get(5);
        String username=usernameText.getText();
        String password=passwordText.getText();
        String reenteredPassword=repasswordText.getText();
        if(dataManager.isExistingUsername(username)){
            homeScreen.createProfileFeedback("Username invalid");
        }else if(!password.equals(reenteredPassword))
            homeScreen.createProfileFeedback("Passwords don't match");
        else{
            homeScreen.clearCreateErrorMessage();
            Profile p=new Profile(username, MD5.encryptPassword(password), dataManager.getModeNames());
            dataManager.addProfile(p);
            Path path= Paths.get(propertyManager.getPropertyValue(WORK_FILE)).toAbsolutePath();
            saveProfile(path);
            dataManager.setCurrentProfile(p);
            homeScreen.getCreateProfileStage().close();
            homeScreen.displayModeToolbar(username);
          //  workspace.setCenter(HOME)
        }

        usernameText.setText("");
        passwordText.setText("");
        repasswordText.setText("");
        workspace.setScreenState(Workspace.ScreenState.HOME_MODE_SCREEN);
    }

    public void handleHelpRequest(){
        Workspace w=(Workspace) appTemplate.getWorkspaceComponent();
        w.getHelpScreen().showScreen();
    }


    public void handleModeSelection(String mode){

        PropertyManager propertyManager= PropertyManager.getManager();
        Path path= Paths.get(propertyManager.getPropertyValue(WORK_FILE)).toAbsolutePath();
        DataManager dm=(DataManager)appTemplate.getDataComponent();
        Workspace w= (Workspace)appTemplate.getWorkspaceComponent();

       // GameDataFile gdf= (GameDataFile) appTemplate.getFileComponent();
        if(!mode.equals("")) {
            dm.getCurrentProfile().setCurrentMode(mode);
            try {
                appTemplate.getFileComponent().saveData(dm, path);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //w.setScreenState(Workspace.ScreenState.LEVEL_SCREEN);
        }

        w.screenTransition(w.LEVEL);


    }

    public void saveProfile(Path to){

        try {
            appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), to);
        } catch (IOException e) {
            e.printStackTrace();
        }
        workFile = to;
    }



}

