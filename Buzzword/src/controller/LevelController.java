package controller;

import apptemplate.AppTemplate;
import data.DataManager;
import gui.GameplayScreen;
import gui.Workspace;

/**
 * Created by jamie on 11/7/2016.
 */
public class LevelController extends MainController {

    AppTemplate appTemplate;
    public LevelController(AppTemplate appTemplate){
        this.appTemplate=appTemplate;
    }

    public void handleLevelSelect(int level){
        DataManager dm=(DataManager) appTemplate.getDataComponent();
        dm.setCurrentLevel(level);
        Workspace workspace=(Workspace)appTemplate.getWorkspaceComponent();
        workspace.screenTransition(workspace.GAMEPLAY);
    }


    public void handleHomeRequest(){
        Workspace w=(Workspace)appTemplate.getWorkspaceComponent();

        w.screenTransition(w.HOME_MODE);
    }




}
