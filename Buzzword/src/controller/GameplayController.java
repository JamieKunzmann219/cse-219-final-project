package controller;

import apptemplate.AppTemplate;
import data.DataManager;
import gui.GameplayScreen;
import gui.Workspace;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

import javafx.scene.text.Text;


import java.util.ArrayList;
import java.util.HashSet;

import java.util.concurrent.locks.ReentrantLock;




/**
 * Created by jamie on 11/7/2016.
 * @author Jamie Kunzmann
 */
public class GameplayController extends MainController{

    public enum GameState{
        NOT_STARTED,
        PAUSED,
        UN_PAUSED,
        ENDED
    }
    private ReentrantLock reentrantLock;
    private AppTemplate appTemplate;
    private GameState   gamestate;
    private int cumulativeScore;
    private boolean[][] isSelected;
    private boolean noLettersSelected;
    private final int GRID_WIDTH=4;
    private final int GRID_HEIGHT=4;
    private ArrayList<Node> draggedHighlights;
    private CountdownTimer timer;
    private final int gameTime=90;

    private ArrayList<TreeView<Button>> paths;
    private HashSet<TreeItem<Button>> justAdded;
    private HashSet<TreeItem<Button>> roots;
    private HashSet<TreeItem<Button>> childrenAdded;


    private int previousNodeX=-1;
    private int previousNodeY=-1;
    public GameplayController(AppTemplate appTemplate){
        this.appTemplate=appTemplate;
        this.gamestate=GameState.NOT_STARTED;
        isSelected=new boolean[GRID_WIDTH][GRID_HEIGHT];
        draggedHighlights=new ArrayList<Node>();
        timer=new CountdownTimer(gameTime,this);

        noLettersSelected=true;
        paths=new ArrayList<TreeView<Button>>();
        roots=new HashSet<TreeItem<Button>>();
        childrenAdded=new HashSet<>();
        justAdded=new HashSet<>();

    }

    public CountdownTimer getTimer(){return timer;}

    public void setGameState(GameState state){gamestate=state;}
    public void setCumulativeScore(int score){cumulativeScore=score;}

    public GameState getGameState(){return gamestate;}
    /**
     * letter is uppercase
     */
    public void handleLetterTyped(char letter){
        if(gamestate==GameState.UN_PAUSED){
            Workspace w=(Workspace) appTemplate.getWorkspaceComponent();
            GameplayScreen gs=w.getGameplayScreen();
            Text lettersGuessed=gs.getLettersGuessed();

            if(noLettersSelected){
                justAdded=new HashSet<>();
                // paths=gs.selectAllOccurences(letter);
                roots=gs.addAllOccurences(letter);
                if(roots.size()==0)
                    lettersGuessed.setText("");
                else{
                    lettersGuessed.setText(Character.toString(letter).toLowerCase());
                    for(TreeItem<Button> t: roots)
                        justAdded.add(t);
                    //if(paths.size()!=0){
                    if(justAdded.size()>0){
                        noLettersSelected=false;
                    }
                }
            }else{
                findNewPaths(letter);
                if(roots.size()==0){
                    noLettersSelected=true;
                    lettersGuessed.setText("");
                }else{
                    lettersGuessed.setText(lettersGuessed.getText().concat(Character.toString(letter).toLowerCase()));
                }
            }
            if(noLettersSelected){
                roots.clear();
                lettersGuessed.setText("");
            }


        }


    }


    private boolean findNewPaths(char letter){
        Workspace w=(Workspace) appTemplate.getWorkspaceComponent();
        GameplayScreen gs=w.getGameplayScreen();
        boolean newPath=false;
        boolean childAdded=false;
        //childrenAdded=new HashSet<>();
        for(TreeItem<Button> t: justAdded){
            childAdded=addAdjacent(t, letter, childrenAdded);
            if(newPath==false){
                newPath=childAdded;
            }

        }
        if(childrenAdded.size()==0){
            resetWordSelection(gs.getLettersGuessed());
        }else{
            for(TreeItem<Button> node: justAdded){
                if(node.getChildren().size()==0){
                    fixGraph(node);
                }
            }
        }
        justAdded=new HashSet<>();
        for(TreeItem c: childrenAdded)
            justAdded.add(c);
        childrenAdded=new HashSet<>();
        return newPath;
    }

    private boolean inUsedPath(Button b, TreeItem<Button> node){

        TreeItem<Button> current;
        for(TreeItem<Button> n: justAdded){
            if(n!=node)
            {
                if(n.getChildren().size()>0){
                    for(TreeItem<Button> c: n.getChildren()){
                        if(c.getValue()==b)
                            return true;
                    }
                }
                current=n;
                while(current!=null){
                    if(current.getValue()==b &&current.getChildren().size()>0)
                        return true;
                    current=current.getParent();
                }
            }
        }
        return false;


    }
    private void fixGraph(TreeItem<Button> node){

        TreeItem<Button> current=node;
        boolean cont=true;
        while(cont &&current!=null){
            if(current.getChildren().size()==0 && !inUsedPath(current.getValue(), node)) {
                TreeItem<Button> parent = current.getParent();
                current.getValue().setEffect(null);
                if (parent == null) {
                    roots.remove(current);
                    cont = false;
                }
                else {
                    parent.getChildren().remove(current);
                    current = parent;
                }
            }else cont=false;
        }

    }
    private void resetPaths(){

        Workspace w=(Workspace) appTemplate.getWorkspaceComponent();
        GameplayScreen gs=w.getGameplayScreen();
        gs.unHighlightAllButtons();
        noLettersSelected=true;
    }

    private boolean addAdjacent(TreeItem<Button> node, char letter, HashSet<TreeItem<Button>> childrenAdded){
        boolean adjacentFound=false;
        Workspace w=(Workspace) appTemplate.getWorkspaceComponent();
        GameplayScreen gs=w.getGameplayScreen();
        Button b= node.getValue();

        //must be  uppercase to compare to letters in grid

        Button[][] buttons=gs.getLetterButtons();
        int x=0;
        int y=0;

        //finds the button in the matrix
        matrix_loop:
        for(int i=0;i<buttons.length;i++){
            for(int j=0;j<buttons[0].length;j++){
                if(buttons[i][j]==b){
                    x=i;
                    y=j;
                    break matrix_loop;
                }
            }
        }

        for(int i=x-1;i<=x+1 && i<buttons.length;i++){
            for(int j=y-1;j<=y+1&&j<buttons[0].length;j++){
                //checks that its within button range and adjacent
                if(i>= 0&& j>=0 && (i!=x || j!=y)){
                    //if adjacent button has the typed letter
                    if(buttons[i][j].getText().charAt(0)==letter){
                        if(!alreadyUsed(buttons[i][j],node)){
                            adjacentFound=true;
                            gs.highlightButton(i,j);
                            TreeItem<Button> newNode=new TreeItem<Button> (buttons[i][j]);
                            node.getChildren().add(newNode);
                            childrenAdded.add(newNode);
                        }

                    }
                }
            }
        }

        return adjacentFound;

    }
    private boolean alreadyUsed(Button b, TreeItem<Button> parent){
        TreeItem<Button> current =parent;
        while(current!=null){
            if(current.getValue()==b){
                return true;
            }
            current=current.getParent();
        }
        return false;
    }

    public void handleDragOverLetter(Button button, int x, int y, String letter){
        if(gamestate==GameState.UN_PAUSED||gamestate==GameState.NOT_STARTED ){
            reentrantLock=new ReentrantLock();
            reentrantLock.lock();
            DataManager dm=(DataManager)appTemplate.getDataComponent();
            Workspace w=(Workspace)appTemplate.getWorkspaceComponent();
            GameplayScreen gs=w.getGameplayScreen();
            Text lettersGuessed=gs.getLettersGuessed();
            try{
                //ensures letter is adjacent to previous letter or is the first letter chosen
                if((previousNodeY==-1 &&previousNodeX==-1) ||((previousNodeX!=x ||previousNodeY!=y) && (Math.abs(previousNodeX-x)<=1 && Math.abs(previousNodeY-y)<=1))) {
                    if (isAlreadyUsed(button)) {
                        resetWordSelection(lettersGuessed);
                    } else {
                        lettersGuessed.setText(lettersGuessed.getText().concat(letter));
                        draggedHighlights.add(button);
                        isSelected[x][y] = true;
                        noLettersSelected = false;

                        //highlights line connecting to previous selected node if there is one
                        if (previousNodeX >= 0 && previousNodeY >= 0) {

                            //path must be adjacent vertical or horizontal
                            if (previousNodeX == x || previousNodeY == y) {
                                draggedHighlights.add(gs.highlightLine(previousNodeX, previousNodeY, x, y));
                            }

                        }
                        //highlights button being dragged over
                        gs.highlightButton(x, y);
                        previousNodeX = x;
                        previousNodeY = y;
                    }
                }
            }finally {
                reentrantLock.unlock();
            }
        }

    }
    private boolean isAlreadyUsed(Button b){
        for(Node n: draggedHighlights){
            if(n==b)
                return true;
        }
        return false;
    }


    public void handleWordSelection(Text lettersGuessed){
        if(timer.getTimerState().equals(CountdownTimer.TimerState.RUNNING)){
            Workspace w=(Workspace) appTemplate.getWorkspaceComponent();
            GameplayScreen gs=(GameplayScreen) w.getGameplayScreen();
            reentrantLock=new ReentrantLock();
            reentrantLock.lock();
            try{
                DataManager dm=(DataManager) appTemplate.getDataComponent();
                HashSet<String> validWords=dm.getValidWordsInGrid();
                String word=lettersGuessed.getText();
                if(validWords.contains(word)){
                    validWords.remove(word);
                    int wordScore=dm.calculateWordScore(word);
                    cumulativeScore+=wordScore;
                    gs.updateScoreDisplay(word, wordScore, cumulativeScore);
                }

                resetWordSelection(lettersGuessed);
            }finally{
                reentrantLock.unlock();
            }
        }

    }
    private void resetWordSelection(Text lettersGuessed){
        lettersGuessed.setText("");

        //resets dragged word selection in grid
        unHighlight();

        /**
        for(int i=0;i<isSelected.length;i++){
            for(int j=0;j<isSelected[0].length;j++)
                isSelected[i][j]=false;
        }
         */
        //resets typed word selection in grid
        resetPaths();

        noLettersSelected=true;
    }

    public void handlePlayPauseRequest(GameplayScreen gameplayScreen){
        if(gamestate==GameState.PAUSED || gamestate==GameState.NOT_STARTED ||gamestate==GameState.ENDED){
            handlePlayRequest(gameplayScreen);
        } else handlePauseRequest(gameplayScreen);
    }

    public void handleNextLevelRequest(GameplayScreen gs){
        DataManager dm=(DataManager)appTemplate.getDataComponent();
        resetWordSelection(gs.getLettersGuessed());
        if(gamestate!=GameState.NOT_STARTED)
            timer.reset();
        dm.setCurrentLevel(dm.getCurrentLevel()+1);
        gs.initScreen();
        cumulativeScore=0;

        gamestate=GameState.NOT_STARTED;
    }
    public void handleReplayRequest(GameplayScreen gs){
        Workspace w=(Workspace) appTemplate.getWorkspaceComponent();
        if(w.getScreenState()== Workspace.ScreenState.GAMEPLAY_SCREEN)
        {
            cumulativeScore=0;
            resetWordSelection(gs.getLettersGuessed());
            setupAndStartNewGame(gs);
            timer.startTimer();
            gs.resumeOrStartGraphics();
            setGameState(GameState.UN_PAUSED);
        }


    }
    public void handlePlayRequest(GameplayScreen gs){
        if(gamestate==GameState.ENDED ){
            setupAndStartNewGame(gs);
            timer.startTimer();
        }
        else if(gamestate==GameState.PAUSED)
            timer.unpause();
        else
            timer.startTimer();

        gs.resumeOrStartGraphics();
        setGameState(GameState.UN_PAUSED);

    }

    public void handleHomeRequest(){
        Workspace w=(Workspace)appTemplate.getWorkspaceComponent();
        DataManager dm=(DataManager)appTemplate.getDataComponent();
        GameplayScreen gs=w.getGameplayScreen();
        cumulativeScore=0;
        dm.clearGameplayData();
        gs.resetData(false);
        gs.unHighlightAllButtons();
        setGameState(GameState.NOT_STARTED);
        timer.reset();
        w.screenTransition(w.HOME_MODE);
    }


    public void handlePauseRequest(GameplayScreen gs){
        if(gamestate!=GameState.NOT_STARTED && gamestate!=GameState.ENDED) {
            timer.pause();
            gs.pauseOrEndGraphics();
            setGameState(GameState.PAUSED);
        }
    }

    public void handleTimeExpired(){
        reentrantLock=new ReentrantLock();
        reentrantLock.lock();
        try {
            Workspace w=(Workspace) appTemplate.getWorkspaceComponent();
            DataManager dm=(DataManager) appTemplate.getDataComponent();
            GameplayScreen gs=w.getGameplayScreen();

            resetWordSelection(gs.getLettersGuessed());
            analyzeGameData(gs);
            //pausedOrEndGraphics
            setGameState(GameState.ENDED);


            //reset cum score
            cumulativeScore=0;
        }finally{
            reentrantLock.unlock();
        }

    }

    /**
     * pre: Current game has ended. data has not been reset yet
     */
    private void analyzeGameData(GameplayScreen gs) {
        DataManager dm = (DataManager) appTemplate.getDataComponent();
        //game stats will have special message if score is personal best
        boolean needsSaving=false;
        boolean isPersonalBest=dm.isPersonalBest(cumulativeScore);
        boolean levelCleared=cumulativeScore>=dm.getTargetScore();
       if(isPersonalBest){
           needsSaving=true;
       }
       if(levelCleared){
           dm.getCurrentProfile().updateNumLevelsCompleted(dm.getCurrentLevel());
           needsSaving=true;
       }
       if(needsSaving) super.saveData(dm, appTemplate);

        gs.showEndGraphics(isPersonalBest,levelCleared, dm.getValidWordsInGrid(), dm.getMaxScore() );

    }

    private void unHighlight(){
        while(draggedHighlights.size()!=0){
            draggedHighlights.get(0).setEffect(null);
            draggedHighlights.remove(0);
        }
        previousNodeX=-1;
        previousNodeY=-1;
    }
    public void setupAndStartNewGame(GameplayScreen gs){
        if(gamestate!=GameState.NOT_STARTED)
            timer.reset();
        gs.reinitialize();
    }
}
