package controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 * Created by jamie on 12/3/2016.
 */
public class CountdownTimer {
    public enum TimerState{
        NOT_STARTED,
        PAUSED,
        RUNNING,
        EXPIRED;
    }
    private GameplayController gameController;
    private TimerState timerState;
    private int startTime;
    private int currentTime;
    private Timeline timeline;
    private static Label secondsLeft;
    private static Label secondsLabel;

    public CountdownTimer(int startTime, GameplayController gc){
        this.startTime=startTime;
        currentTime=startTime;
        secondsLeft=new Label(""+currentTime);
        timerState=TimerState.NOT_STARTED;
        gameController=gc;
        secondsLabel=new Label("seconds");

    }

    public static Label getTimeLabel(){return secondsLeft;}
    public static Label getSecondsLabel() {return secondsLabel;}
    public void startTimer(){
        timeline=new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        secondsLeft.setTextFill(Color.LIMEGREEN);
        secondsLeft.setText(""+currentTime);
        secondsLabel.setTextFill(Color.LIMEGREEN);
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1), e-> updateTimeLabel()));
        timeline.playFromStart();
        timerState=TimerState.RUNNING;

    }
    private void updateTimeLabel(){
        currentTime--;
        if(currentTime==startTime/2) {
            secondsLeft.setTextFill(Color.YELLOW);
            secondsLabel.setTextFill(Color.YELLOW);
        }
        if(currentTime==10) {
            secondsLeft.setTextFill(Color.RED);
            secondsLabel.setTextFill(Color.RED);
        }

        secondsLeft.setText(""+currentTime);
        if(currentTime==0) {
            timeline.stop();
            timerState=TimerState.EXPIRED;
            gameController.handleTimeExpired();
            currentTime=startTime;
        }
    }
    public void reset(){
        if(timeline!=null)
            timeline.stop();
        resetCurrentTime();
    }


    private void resetCurrentTime(){
        currentTime = startTime;
        secondsLeft.setText("" + currentTime);
        secondsLeft.setTextFill(Color.LIMEGREEN);
        secondsLabel.setTextFill(Color.LIMEGREEN);
        timerState=TimerState.NOT_STARTED;
    }

    public void pause(){
        timeline.pause();
        timerState=TimerState.PAUSED;
    }
    public void unpause()
    {
        timeline.play();
        timerState=TimerState.RUNNING;
    }

    public TimerState getTimerState(){return timerState;}


}
