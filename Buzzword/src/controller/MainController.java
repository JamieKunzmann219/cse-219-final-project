package controller;

import apptemplate.AppTemplate;
import components.AppFileComponent;
import data.DataManager;
import data.MD5;
import data.Profile;
import gui.GameplayScreen;
import gui.ProfileSettingsScreen;
import gui.Workspace;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import propertymanager.PropertyManager;
import ui.YesNoDialogSingleton;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static buzzword.BuzzwordProperties.*;

/**
 * Created by jamie on 11/7/2016.
 * @author Jamie Kunzmann
 */
public class MainController implements FileController{
    LevelController levelController;
    HomeController homeController;
    GameplayController gameplayController;
    AppTemplate appTemplate;
    Workspace workspace;

    public MainController(AppTemplate appTemplate){
        this.appTemplate=appTemplate;
       // workspace= (Workspace) appTemplate.getWorkspaceComponent();
        levelController=new LevelController(appTemplate);
        homeController=new HomeController(appTemplate);
        gameplayController=new GameplayController(appTemplate);


    }

    public MainController() {
    }

    public void handleLogoutRequest(ProfileSettingsScreen screen){
        DataManager dm=(DataManager) appTemplate.getDataComponent();
        Workspace w=(Workspace) appTemplate.getWorkspaceComponent();
        if(w.getScreenState().equals(Workspace.ScreenState.GAMEPLAY_SCREEN)){
            YesNoDialogSingleton singleton= YesNoDialogSingleton.getSingleton();
            PropertyManager propertyManager=PropertyManager.getManager();
            singleton.show(propertyManager.getPropertyValue(LOGOUT_TITLE),
                    propertyManager.getPropertyValue(LOGOUT_MESSAGE));
            if(singleton.getSelection().equals(YesNoDialogSingleton.YES)) {
                gameplayController.getTimer().reset();
                GameplayScreen gs=w.getGameplayScreen();
                gameplayController.setGameState(GameplayController.GameState.NOT_STARTED);
                dm.clearGameplayData();
                gameplayController.setCumulativeScore(0);
                gs.unHighlightAllButtons();
                gs.resetData(false);
            }
            else {
                return;
            }
        }
        dm.setCurrentProfile(null);
        w.getHomeScreen().getGameModeMenu().setValue(null);
        w.screenTransition(w.HOME_LOGIN);
        screen.reset();
        screen.close();
    }
    public HomeController getHomeController(){return this.homeController;}

    public GameplayController getGameplayController(){return this.gameplayController;}

    public LevelController getLevelController(){return this.levelController;}

    @Override
    public void handleNewRequest() {
        
    }

    @Override
    public void handleSaveRequest() throws IOException {

    }

    @Override
    public void handleLoadRequest() throws IOException {

    }

    @Override
    public void handleExitRequest() {
        YesNoDialogSingleton singleton=YesNoDialogSingleton.getSingleton();
        DataManager dm=(DataManager)appTemplate.getDataComponent();
        Workspace w =(Workspace)appTemplate.getWorkspaceComponent();
        GameplayScreen gs=w.getGameplayScreen();
        PropertyManager propertyManager=PropertyManager.getManager();

        if(w.getScreenState().equals(Workspace.ScreenState.HOME_LOGIN_SCREEN))
            System.exit(0);
        if(w.getScreenState().equals(Workspace.ScreenState.GAMEPLAY_SCREEN))
            if(gameplayController.getGameState().equals(GameplayController.GameState.UN_PAUSED))
                gameplayController.handlePauseRequest(gs);
                gameplayController.setGameState(GameplayController.GameState.PAUSED);
        singleton.show(propertyManager.getPropertyValue(EXIT_TITLE), propertyManager.getPropertyValue(EXIT_MESSAGE));

        if (singleton.getSelection().equals(YesNoDialogSingleton.YES))
            System.exit(0);

    }

    public void handleHomeRequest(){
        Workspace w=(Workspace)appTemplate.getWorkspaceComponent();

        w.screenTransition(w.HOME_MODE);
    }

    public void saveData(DataManager dm, AppTemplate app){
        PropertyManager propertyManager= PropertyManager.getManager();
        Path path= Paths.get(propertyManager.getPropertyValue(WORK_FILE)).toAbsolutePath();
        try {
            AppFileComponent file=app.getFileComponent();
            file.saveData(dm, path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleAccountRequest(){
        Workspace w=(Workspace) appTemplate.getWorkspaceComponent();
        DataManager dm=(DataManager)appTemplate.getDataComponent();
        Profile p=dm.getCurrentProfile();
        if(w.getScreenState()== Workspace.ScreenState.GAMEPLAY_SCREEN){
            gameplayController.handlePauseRequest(w.getGameplayScreen());
        }
        w.getProfileSettingsScreen().setStats(p);
        w.getProfileSettingsScreen().showScreen();
    }

    public void handleProfileEdited(PasswordField passwordTxt, ProfileSettingsScreen screen){
        PropertyManager propertyManager=PropertyManager.getManager();
        DataManager dataManager=(DataManager) appTemplate.getDataComponent();
        Profile p=dataManager.getCurrentProfile();
        String password=passwordTxt.getText();
        if(!password.equals("")){
            p.setPassword(MD5.encryptPassword(password));
            Path path= Paths.get(propertyManager.getPropertyValue(WORK_FILE)).toAbsolutePath();
            saveProfile(path);
        }
        screen.reset();
        screen.close();
    }
    public void saveProfile(Path to){

        try {
            appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), to);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
