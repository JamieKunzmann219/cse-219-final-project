# README #

PROJECT:
BuzzWord

DESCRIPTION:
BuzzWord is a variation of the popular word game Boggle, where players are challenged to find as many valid words in a letter grid as possible before time runs out. BuzzWord also adds the aspect of different game modes that are defined by categories of valid words, such as English Dictionary, Names, and Verbs. 

Given a Software Design Specification (SRS), my first step was to create a [Software Design Document](https://docs.google.com/document/d/183JAyMHZFTi-Agd2ir2nurvzS7koy6EJAk_Kxv_R3vY/edit?usp=sharing). This was done using a top-down design approach. I then implemented my ideas using JavaFX, HTML, and CSS.

I used a variety of data structures to efficiently complete this project, including Trees. I also implemented a recursive method in order to find all of the valid words in the grid. This method is called recursivelyFindWords and can be found in Buzzword/src/data/DataManager.Java


Note: This project was completed independently using a framework provided by my CSE 219 professor. 